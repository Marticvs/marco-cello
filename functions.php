<?php

/**
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ogiadv
 */

if ( ! function_exists( 'ogiadv_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */

// Register Custom Navigation Walker
//require_once('wp_bootstrap_navwalker.php');


function ogiadv_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Ponte Vecchio, use a find and replace
	 * to change '' to the name of your theme in all the template files.
	 */

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );
}
endif;

// Register Custom Navigation Walker
require_once('wp-bootstrap-navwalker.php');

function wpbasics_theme_setup() {
	//Nav Menus
	register_nav_menus( array(
			'primary' => __( 'Primary Menu')
	) );
}

function ogiadv_scripts() {

	/* bootstrapstyle */
	wp_enqueue_style( 'bootstrapstyle', get_template_directory_uri() . '/css/bootstrap.css' );
	/* */
	wp_enqueue_style( 'animate', get_template_directory_uri() . '/css/animate.css' );
	/* animate.css */
	wp_enqueue_style( 'icomoon', get_template_directory_uri() . '/css/icomoon.css' );
	/* icomoon.css */
	wp_enqueue_style( 'simple-line', get_template_directory_uri() . '/css/simple-line-icons.css' );
	/* simple-line-icons.css */
	wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/css/owl.carousel.min.css' );


	/* owl.carousel.min.js */
	wp_enqueue_script( 'jQuery', get_template_directory_uri() . '/js/jquery.min.js' );
	/* owl.carousel.min.js */
    wp_enqueue_script( 'bootstrap-script', get_template_directory_uri() . '/js/bootstrap.min.js' );
    /* modernizr.js */
    wp_enqueue_script( 'bootstrap-script', get_template_directory_uri() . '/js/modernizr-2.6.2.min.js' );


	wp_enqueue_style( 'ogiadv-style', get_stylesheet_uri() );

}
add_action( 'wp_enqueue_scripts', 'ogiadv_scripts' );


