<?php 
	//Template Name: Investimenti
?>

<?php wp_head(); ?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
<meta name="description" content="Entra a far parte del mio business! Rapido, facile e sicuro, il suo obiettivo è farti generare entrate automatiche con investimenti minimi." />

<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,600,400italic,700' rel='stylesheet' type='text/css'>

 <header role="banner" id="fh5co-header">
			<div class="container">
				<!-- <div class="row"> -->
			    <nav class="navbar navbar-default">
		        <div class="navbar-header">
		        	<!-- Mobile Toggle Menu Button -->
					<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
		          	<a class="navbar-brand" href="home"><img class="logo" src="<?php echo get_template_directory_uri(); ?>/images/logoCello.png"></a>
		        </div>

		       
		        <div id="navbar" class="navbar-collapse collapse">
		          <ul class="nav navbar-nav navbar-right">
		          	<li><a href="#" data-nav-section="press"><span>CONTATTI</span></a></li>
		            <li><a href="home" data-nav-section="home"><span>TORNA ALLA HOME</span></a></li>
		          </ul>
		        </div>
			    </nav>
			  <!-- </div> -->
		  </div>
</header>

<!-- video in background -->
	<div id="slider" data-section="home">
		<div class="video-container">
			   <video autoplay playsinline loop muted>
				     <source src="<?php echo get_template_directory_uri(); ?>/video/video.mp4" type="video/mp4">
				     Your browser does not support the video tag.
			   </video>
		     <div class="overlay-desc">
		        <div class="col-md-12 section-heading text-center">
					<div class="heading-titolo">SE CI CREDI SI REALIZZA</div>
					<div class="row">
						<div class="col-md-8 col-md-offset-2 to-animate">
							<h3 style="color: #fff;">Esplora nuove opportunità, fai viaggiare il tuo business</h3>
						</div>
					</div>
						<div class="to-animate-2"><a href="#" data-nav-section="press" class="btn btn-primary btn-lg">CONTATTAMI</a></div>
				</div>
		     </div>
		</div>
			 
	</div>
<!-- video in background -->

<div id="fh5co-testimonials" data-section="testimonials">	
		  <div class="container">	
			<div class="col-md-12 section-heading text-center">
					<img src="<?php echo get_template_directory_uri(); ?>/images/servizi/investimenti.png" alt="investimenti">
						<h2 class="to-animate">Investimenti</h2>
						<div class="row">
							<div class="col-md-8 col-md-offset-2 subtext to-animate" style="text-align: left; color: white;">
								<div class="descrizione-servizi">
									<p>Quanto devo investire? Per quanto tempo? E soprattutto…. quanto ci guadagno?</p>
									<p>Esiste un vero e proprio identikit di chi vorrà investire in un business destinato a generare delle entrate automatiche.</p>
									<p>Quella persona potresti essere tu! </p>
									<p>Se hai dubbi su come procedere, quali rendimenti netti aspettarti e a quali rischi potresti esporre il tuo capitale, contattami! Ti aiuterò a scegliere la strada più rapida, facile e sicura per il tuo investimento.</p>
								</div>
							</div>
						</div>
				</div>
			</div>
	</div>

	<div id="fh5co-press" data-section="press">
		<div class="container">
			<div class="row">
				<div class="col-md-12 section-heading text-center">
					<h2 class="single-animate animate-press-1">CONTATTI</h2>
				</div>
			</div>

<div class="col-md-12 section-heading text-center">
					<div class="row">
						<div class="col-md-8 col-md-offset-2 to-animate fadeInUp animated">
							<h3 style="text-align: center;">Scrivimi all'indirizzo email: <br>mcello1012@gmail.com</h3>
						</div>
					</div>
				</div>

				<!-- <div class="col-md-6">
					<div class="fh5co-press-item to-animate">
						<div class="fh5co-press-img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/img_10.jpg);">
						</div>
						<div class="fh5co-press-text">
							<h3 class="h2 fh5co-press-title">Esempio4 <span class="fh5co-border"></span></h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis eius quos similique suscipit dolorem cumque vitae qui molestias illo accusantium...</p>
							<p><a href="#" class="btn btn-primary btn-sm">Learn more</a></p>
						</div>
					</div>
				</div> -->

			</div>
		</div>
	</div>

<!-- barra icone social -->
		<div class="fixed-info">
				<div class="row">
					<div class="col-md-12 text-center">
						<ul class="social social-circle" style="margin-top: 1%">
							<li><a href="https://www.facebook.com/marcocellobusinessman"><i class="icon-facebook"></i></a></li>
							<li><a href="https://www.youtube.com/channel/UCSqhuFfohkbalFZs23TrGEw"><i class="icon-youtube"></i></a></li>
							<li><a href="https://www.linkedin.com/in/marco-cello-915204125/"><i class="icon-linkedin"></i></a></li>				
							<li><a href="https://www.instagram.com/marcocello_businessman/"><i class="icon-instagram"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="row row-bottom-padded-sm">
					<div class="col-md-12">
						<p class="copyright text-center" style="color:#fff";>&copy; 2017 - <a href="http://www.ogiadvertising.it/">ogiadvertising</a> </p>
					</div>
				</div>
		</div>
	<!-- barra icone social -->

<?php get_footer();
?>	