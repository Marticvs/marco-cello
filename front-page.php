<?php
	get_header();
?>


<!-- video in background -->
	<div id="slider" data-section="home">
		<div class="video-container">
			   <video autoplay playsinline loop muted>
				     <source src="<?php echo get_template_directory_uri(); ?>/video/video.mp4" type="video/mp4">
				     Your browser does not support the video tag.
			   </video>
		     <div class="overlay-desc">
		        <div class="col-md-12 section-heading text-center">
					<div class="heading-titolo">SE CI CREDI SI REALIZZA</div>
					<div class="row">
						<div class="col-md-8 col-md-offset-2 to-animate">
							<h3 style="color: #fff;">Esplora nuove opportunità, fai viaggiare il tuo business</h3>
						</div>
					</div>
						<div class="to-animate-2"><a href="#" data-nav-section="press" class="btn btn-primary btn-lg">CONTATTAMI</a></div>
				</div>
		     </div>
		</div>
			 
	</div>
<!-- video in background -->
	<div id="fh5co-about-us" data-section="about">
		<div class="container">
			<div class="row row-bottom-padded-lg" id="about-us">
				<div class="col-md-12 section-heading text-center">
					<div class="row">
						<div class="col-md-8 col-md-offset-2 to-animate">
							<h3 style="text-align: justify;">Ciao! Mi chiamo Marco Cello e sono un imprenditore che
							 dal 2012 vive e lavora in Venezuela, aiutando le aziende italiane ad esportare i loro prodotti in questo splendido paese.</h3>
						</div>
					</div>
				</div>
				<div class="col-md-6 to-animate">
					<img src="<?php echo get_template_directory_uri(); ?>/images/fotoprofilo2.jpg" class="img-responsive img-rounded" alt="Free HTML5 Template">
				</div>
				<div class="col-md-5 to-animate">
					<p style="font-size: 19px;">La mia profonda conoscenza del territorio e delle leggi venezuelane sono un fattore che fa la differenza negli affari!</p>
					<p style="font-size: 19px;">Conosco il mondo delle imprese dal 2008, anno in cui ho costituito un’azienda molto attiva su tutto il territorio nazionale nel settore creditizio. Inoltre, nel 2011 ho partecipato all’acquisizione di un’azienda con sede a Miami, quotata in borsa al Nasdaq, finalizzata all’apertura in franchising italiani di negozi di ristorazione espressa.</p>
					<p style="font-size: 19px;">Oggi sono fondatore e socio di maggioranza di Pamar International C.A. e ogni giorno aiuto tanti investitori a realizzare importanti profitti, sfruttando soprattutto le opportunità che esistono nel settore dell’import/export tra l’Italia, Panama e il Venezuela.</p>
				</div>
			</div>
			<div class="row" id="team">
				<div class="col-md-12 section-heading text-center to-animate">
					<h2>perchè credere in me</h2>
				</div>
				<div class="col-md-12">
					<div class="row row-bottom-padded-lg">
						<div class="col-md-4 text-center to-animate">
							<div class="person">
								<img src="<?php echo get_template_directory_uri(); ?>/images/motivo-1.png" class="img-responsive img-rounded" alt="Person">
								<!-- <h3 class="name">Motivo 1</h3> -->
								<p style="margin-top: 4em;">Diverse son le aziende che hanno deciso di lavorare con me. Tutte hanno visto un interessante ritorno medio del loro investimento, oltre ad aver aggiunto un preziosissimo bagaglio di esperienze sul campo.</p>
							</div>
						</div>
						<div class="col-md-4 text-center to-animate">
							<div class="person">
								<img src="<?php echo get_template_directory_uri(); ?>/images/motivo-2.png" class="img-responsive img-rounded" alt="Person">
								<!-- <h3 class="name">Motivo 2</h3> -->
								<p style="margin-top: 4em;">Nel 2013 sono stato nominato “Direttore delle alleanze strategiche tra Italia e Venezuela” da Jaime Pérez, Presidente della Cámara de Comercio y Empresarios del Mercosur Capítulo Venezuela. Utilizzo le mie conoscenze e i miei canali preferenziali in questo Paese per migliorare la qualità del tuo lavoro, dei tuoi risultati e della tua azienda!</p>
							</div>
						</div>
						<div class="col-md-4 text-center to-animate">
							<div class="person">
								<img src="<?php echo get_template_directory_uri(); ?>/images/motivo-3.png" class="img-responsive img-rounded" alt="Person">
								<!-- <h3 class="name">Motivo 3</h3> -->
								<p style="margin-top: 4em;">Dal fallimento… alla rinascita!
								Dopo 10 anni di attività con la mia azienda nel campo creditizio e finanziario, ho dovuto chiudere per le condizioni avverse del mercato. Credendo in me stesso, la risorsa più importante che un imprenditore possa avere, ho trasformato la mia vita, producendo benessere per me e per gli altri.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="fh5co-our-services" data-section="services">
		<div class="container">
			<div class="row row-bottom-padded-sm">
				<div class="col-md-12 section-heading text-center">
					<h2 class="to-animate">Servizi</h2>
					<div class="row">
						<div class="col-md-8 col-md-offset-2 to-animate">
							<h3>"Come trasformare la tua attività in un business da sogno"</h3>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="box to-animate">
						<div class="icon colored-1"><span><img src="<?php echo get_template_directory_uri(); ?>/images/servizi/consulenza.png" class="img-responsive img-rounded" alt="consulenza"></span></div>
						<h3>Consulenza</h3>
						<p>Formo e aiuto gli imprenditori che vogliono estendere il loro business e migliorare la propria situazione finanziaria. Cambiamo passo insieme!</p>
						<p><a href="consulenza" class="btn btn-primary btn-sm">scopri di più</a></p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="box to-animate">
						<div class="icon colored-2"><span><img src="<?php echo get_template_directory_uri(); ?>/images/servizi/investimenti.png" class="img-responsive img-rounded" alt="export"></span></div>
						<h3>Investimenti</h3>
						<p>Il mio business è finalizzato a farti generare entrate automatiche con investimenti minimi!  In un mondo globalizzato come quello attuale, esplorare nuovi mercati non è un sogno, è un'opportunità da cogliere al volo!</p>
						<p><a href="investimenti" class="btn btn-primary btn-sm">scopri di più</a></p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="box to-animate">
						<div class="icon colored-2"><span><img src="<?php echo get_template_directory_uri(); ?>/images/servizi/export.png" class="img-responsive img-rounded" alt="investimenti"></span></div>
						<h3>Export</h3>
						<p>La forza dei prodotti Made in Italy valorizzata dall'Export in Venezuela. Studiamo insieme le possibilità che ci vengono offerte dai mercati esteri e portiamo i prodotti della tua azienda in questo splendido Paese!</p>
						<p><a href="export" class="btn btn-primary btn-sm">scopri di più</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="fh5co-testimonials" data-section="testimonials" style="background: #14aecf">		
		<div class="container">
			<div class="row">
				<div class="col-md-12 section-heading text-center">
					<h2 class="to-animate" style="color: white">TESTIMONIANZE</h2>
					<div class="row">
					</div>
				</div>
			</div>
			<div class="row">
<div class="col-md-4">
					<div class="box-testimony to-animate">
						<blockquote>
							<span class="quote"><span><i class="icon-quote-left"></i></span></span>
							<p>&ldquo;Conocer a marco me hizo ver el ejemplo viviente de un verdadero emprendedor, una persona apasionada y persistente, capaz de identificar cualquier oportunidad de negocio y convertirlo en un éxito. Invertir en venezuela y darnos la oportunidad de conocer los sabores y la cultura de Italia a través de los productos de primerísima Calidad de la empresa Pamar International, me hacen expresar mi Más profunda admiración y agradecimiento por tan excelente emprendimiento y dedicación, potenciando el crecimiento económico y generación de fuentes de empleo en nuestro país. Gracias Marco por confiar, valorar y creer en Venezuela.</p>
						</blockquote>
						<p class="author">ing. Yoselin Tavares U.  
							<span class="subtext">Empresa: Tavares Group C.A.<br>Directora Financiera</span>
						</p>
					</div>
				</div>					
<div class="col-md-4">
					<div class="box-testimony to-animate">
						<blockquote>
							<span class="quote"><span><i class="icon-quote-left"></i></span></span>
							<p>&ldquo;Marco è una persona davvero speciale, sempre sorridente e positiva.<br>
							Nonostante le numerose difficoltà che ha dovuto superare nella propria vita professionale, è riuscito a concretizzare il suo sogno ed io ne sono testimone perché ho potuto constatare, direttamente in Venezuela, la sua realtà imprenditoriale e la rete internazionale di collegamenti strategici di cui fa parte.<br>Chiunque cerchi un professionista affidabile per creare delle entrate automatizzate ad alto reddito, trova in Marco la persona giusta perché è il classico “self 
 made man”.</p>
						</blockquote>
						<p class="author">Nicola Lovaglio 
							<span class="subtext">amministratore <a href="http://ogiadvertising.it/" target="_blank"> OGI advertising</a></span>
						</p>
					</div>
				</div>	
                <div class="col-md-4">
					<div class="box-testimony to-animate">
						<blockquote>
							<span class="quote"><span><i class="icon-quote-left"></i></span></span>
							<p>&ldquo;Da quando conosco Marco ho avuto modo di apprezzare le sue indubbie competenze.
Sin da subito si è instaurato un bellissimo rapporto di fiducia.<br>
Il fattore determinante della mia scelta di investire nel suo business è stato l’alta sostenibilità del suo progetto e la sua presenza sul territorio venezuelano.<br>
L’opportunità di generare entrate automatiche, per me, ha rappresentato e rappresenta una concreta alternativa al mio guadagno diretto. La trasparenza delle condizioni sull’investimento ha fatto sì che la mia decisione diventasse “azione”!
”</p>
						</blockquote>
						<p class="author">Natale De Filippo 
							<span class="subtext">Amministratore Unico di Italian Excellence Mark s.r.l.</span>
						</p>
					</div>
				</div>
			<div class="col-md-12 section-heading text-center">
				<a href="#" class="btn btn-default btn-sm">Scopri di più</a>
			</div>
			</div>
		</div>
	</div>

	<div id="fh5co-features" data-section="features">
	<div class="container">
		<div class="row">
				<div class="col-md-12 section-heading text-center">
					<h2 class="single-animate animate-features-1">Blog</h2>
				</div>
			</div>
				<div class="row row-bottom-padded-sm">
											<?php // the query
											   $the_query = new WP_Query( array(
											     'category_name' => 'news',
											      'posts_per_page' => 3,
											   )); 
											?>
											<?php if ( $the_query->have_posts() ) : ?>
											  <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
								<div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12 fh5co-service to-animate">
									<div class="fh5co-icon"><span><img src="<?php echo get_template_directory_uri(); ?>/images/IconaBlog.png" class="img-responsive img-rounded"></span></div>
										<div class="fh5co-desc">
											<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
											<p><?php the_excerpt(); ?></p>
										</div>
								</div>
									<?php endwhile; ?>
									  <?php wp_reset_postdata(); ?>
									  <?php else : ?>
									  <p><?php __('No News'); ?></p>
								<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
</div>

	<div id="fh5co-press" data-section="press">
		<div class="container">
			<div class="row">
				<div class="col-md-12 section-heading text-center">
					<h2 class="single-animate animate-press-1">CONTATTI</h2>
				</div>
			</div>

				<div class="col-md-12 section-heading text-center">
					<div class="row">
						<div class="col-md-8 col-md-offset-2 to-animate fadeInUp animated">
							<div align="center">
								<?php echo do_shortcode('[contact-form-7 id="28" title="Modulo di contatto 1"]'); ?>
							</div>
						</div>
					</div>
				</div>   

				<div class="col-md-12 section-heading text-center">
					<div class="row">
						<div class="col-md-8 col-md-offset-2 to-animate fadeInUp animated">
							<h3 style="text-align: center;"></h3>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<!-- barra icone social -->
		<div class="fixed-info">
				<div class="row">
					<div class="col-md-12 text-center">
						<ul class="social social-circle" style="margin-top: 1%">
							<li><a href="https://www.facebook.com/marcocellobusinessman"><i class="icon-facebook"></i></a></li>
							<li><a href="https://www.youtube.com/channel/UCSqhuFfohkbalFZs23TrGEw"><i class="icon-youtube"></i></a></li>
							<li><a href="https://www.linkedin.com/in/marco-cello-915204125/"><i class="icon-linkedin"></i></a></li>
							<li><a href="https://www.instagram.com/marcocello_businessman/"><i class="icon-instagram"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="row row-bottom-padded-sm">
					<div class="col-md-12">
						<p class="copyright text-center" style="color:#fff";>&copy; 2017 - <a href="http://www.ogiadvertising.it/">OGIadvertising</a> </p>
					</div>
				</div>
		</div>
	<!-- barra icone social -->


<?php get_footer();
?>					