<?php 
	//template name: Single
?>

<body>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,600,400italic,700' rel='stylesheet' type='text/css'>

<?php wp_head(); ?>

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
<link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">


 <header role="banner" id="fh5co-header">
            <div class="container">
                <!-- <div class="row"> -->
                <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <!-- Mobile Toggle Menu Button -->
                    <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
                    <a class="navbar-brand" href="home"><img class="logo" src="<?php echo get_template_directory_uri(); ?>/images/logoCello.png"></a> 
                </div>

               
                <div id="navbar" class="navbar-collapse collapse">
                  <ul class="nav navbar-nav navbar-right">
                    <li><a href="blog" style="color: rgba(0, 0, 0, 0.5);" data-nav-section="home"><span>BLOG</span></a></li>
                  </ul>
                </div>
                </nav>
              <!-- </div> -->
          </div>
</header>

<div id="fh5co-blog" data-section="blog" style="padding: 7em 0">       
 <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="post-preview">

            	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            		<h2 class="post-title">
            		 <?php the_title(); ?>
            		</h2>

            		<div class="post-description">
            		 <?php the_content(); ?>
            		</div>
                
            		 <?php echo get_the_date(); ?>


            	<?php endwhile; ?>
            	<?php endif; ?>
			
				</div>
			</div>
		</div>
	</div>
</div>

<!-- barra icone social -->
    <div class="fixed-info">
        <div class="row">
          <div class="col-md-12 text-center">
           <ul class="social social-circle" style="margin-top: 1%">
							<li><a href="https://www.facebook.com/marcocellobusinessman"><i class="icon-facebook"></i></a></li>
							<li><a href="https://www.youtube.com/channel/UCSqhuFfohkbalFZs23TrGEw"><i class="icon-youtube"></i></a></li>
							<li><a href="https://www.linkedin.com/in/marco-cello-915204125/"><i class="icon-linkedin"></i></a></li>				
							<li><a href="https://www.instagram.com/marcocello_businessman/"><i class="icon-instagram"></i></a></li>
						</ul>
          </div>
        </div>
        <div class="row row-bottom-padded-sm">
          <div class="col-md-12">
            <p class="copyright text-center" style="color:#fff";>&copy; 2017 - <a href="http://www.ogiadvertising.it/">ogiadvertising</a> </p>
          </div>
        </div>
    </div>
  <!-- barra icone social -->

<?php get_footer();
?>	