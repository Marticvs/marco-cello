<?php 
	//Template Name: Consulenza
?>

<?php wp_head(); ?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Aiuto gli imprenditori che vogliono estendere il loro business e migliorare la propria situazione finanziaria con vantaggiosi investimenti all’estero." />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">

<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,600,400italic,700' rel='stylesheet' type='text/css'>

 <header role="banner" id="fh5co-header">
			<div class="container">
				<!-- <div class="row"> -->
			    <nav class="navbar navbar-default">
		        <div class="navbar-header">
		        	<!-- Mobile Toggle Menu Button -->
					<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
		          	<a class="navbar-brand" href="home"><img class="logo" src="<?php echo get_template_directory_uri(); ?>/images/logoCello.png"></a>
		        </div>
		        <div id="navbar" class="navbar-collapse collapse">
		          <ul class="nav navbar-nav navbar-right">
		          	<li><a href="#" data-nav-section="press"><span>CONTATTI</span></a></li>
		            <li><a href="home"><span>BACK TO HOME</span></a></li>
		          </ul>
		        </div>
			    </nav>
			  <!-- </div> -->
		  </div>
	</header>

<!-- video in background -->
	<div id="slider" data-section="home">
		<div class="video-container">
			   <video autoplay playsinline loop muted>
				     <source src="<?php echo get_template_directory_uri(); ?>/video/video.mp4" type="video/mp4">
				     Your browser does not support the video tag.
			   </video>
		     <div class="overlay-desc">
		        <div class="col-md-12 section-heading text-center">
					<div class="heading-titolo">SE CI CREDI SI REALIZZA</div>
					<div class="row">
						<div class="col-md-8 col-md-offset-2 to-animate">
							<h3 style="color: #fff;">Esplora nuove opportunità, fai viaggiare il tuo business</h3>
						</div>
					</div>
						<div class="to-animate-2"><a href="#" data-nav-section="press" class="btn btn-primary btn-lg">CONTATTAMI</a></div>
				</div>
		     </div>
		</div>
			 
	</div>
<!-- video in background -->

	<div id="fh5co-testimonials" data-section="testimonials">	
	  <div class="container">	
		<div class="col-md-12 section-heading text-center">
					<img src="<?php echo get_template_directory_uri(); ?>/images/servizi/consulenza.png" alt="consulenza">
					<h2 class="to-animate">Consulenza</h2>
					<div class="row">
						<div class="col-md-8 col-md-offset-2 subtext to-animate" style="text-align: left;">
							<div class="descrizione-servizi">
								<p>Vivo e lavoro in Venezuela da cinque anni, aiutando gli imprenditori che hanno deciso di investire in questo mercato o esportare qui il Made in Italy.  Jaime Pérez, Presidente della Cámara de Comercio y Empresarios del Mercosur Capítulo Venezuela, mi ha nominato “Direttore delle alleanze strategiche tra Italia e Venezuela”, premiando la mia instancabile attività, finalizzata a formulare ai clienti proposte d'investimento pensate su misura per loro. </p>

								<p>Entrare nel mercato venezuelano non è per niente facile ma, nonostante la grave crisi politica e sociale del paese e la quasi totale assenza di industrie di beni di largo consumo, necessita di ogni genere di prodotto.
								</p>

								<p>Queste minacce significano, per un imprenditore, tante opportunità!
								</p>

								<p>Per questo ogni giorno metto il mio know how a disposizione di chi vuole migliorare la propria situazione finanziaria e accrescere il proprio business attraverso le infinite opportunità offerte dal settore dell’import/export tra l'Italia, Panama e il Venezuela.</p>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>

	<div id="fh5co-press" data-section="press">
		<div class="container">
			<div class="row">
				<div class="col-md-12 section-heading text-center">
					<h2 class="single-animate animate-press-1">CONTATTI</h2>
				</div>
			</div>

<div class="col-md-12 section-heading text-center">
					<div class="row">
						<div class="col-md-8 col-md-offset-2 to-animate fadeInUp animated">
							<h3 style="text-align: center;">Scrivimi all'indirizzo email: <br>mcello1012@gmail.com</h3>
						</div>
					</div>
				</div>

				<!-- <div class="col-md-6">
					<div class="fh5co-press-item to-animate">
						<div class="fh5co-press-img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/img_10.jpg);">
						</div>
						<div class="fh5co-press-text">
							<h3 class="h2 fh5co-press-title">Esempio4 <span class="fh5co-border"></span></h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis eius quos similique suscipit dolorem cumque vitae qui molestias illo accusantium...</p>
							<p><a href="#" class="btn btn-primary btn-sm">Learn more</a></p>
						</div>
					</div>
				</div> -->

			</div>
		</div>
	</div>

	<!-- barra icone social -->
		<div class="fixed-info">
				<div class="row">
					<div class="col-md-12 text-center">
						<ul class="social social-circle" style="margin-top: 1%">
							<li><a href="https://www.facebook.com/marcocellobusinessman"><i class="icon-facebook"></i></a></li>
							<li><a href="https://www.youtube.com/channel/UCSqhuFfohkbalFZs23TrGEw"><i class="icon-youtube"></i></a></li>
							<li><a href="https://www.linkedin.com/in/marco-cello-915204125/"><i class="icon-linkedin"></i></a></li>				
							<li><a href="https://www.instagram.com/marcocello_businessman/"><i class="icon-instagram"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="row row-bottom-padded-sm">
					<div class="col-md-12">
						<p class="copyright text-center" style="color:#fff";>&copy; 2017 - <a href="http://www.ogiadvertising.it/">ogiadvertising</a> </p>
					</div>
				</div>
		</div>
	<!-- barra icone social -->

<?php get_footer();
?>		