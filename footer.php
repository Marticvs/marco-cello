	<footer id="footer" role="contentinfo">
	</footer>
	
	<!-- jQuery -->
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.waypoints.min.js"></script>
	<!-- Owl Carousel -->
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/owl.carousel.min.js"></script>
	<!-- For demo purposes only styleswitcher ( You may delete this anytime ) -->
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.style.switcher.js"></script>
	

	<script>
	$(function(){
		$('#colour-variations ul').styleSwitcher({
			defaultThemeId: 'theme-switch',
			hasPreview: false,
			cookie: {
	          	expires: 30,
	          	isManagingLoad: true
	      	}
		});	
		$('.option-toggle').click(function() {
			$('#colour-variations').toggleClass('sleep');
		});
	});
	</script>
	<!-- End demo purposes only -->



	<!-- Main JS (Do not remove) -->
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/main.js"></script>

	</body>
	<!-- theme by Marco Amodio -->
</html>

