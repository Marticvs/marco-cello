<!DOCTYPE html>

<!-- 
theme by Marco Amodio 
-->
<html class="no-js"> 
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Marco Cello</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Sono Marco Cello e sono un business man. Aiuto chi vuole fare investimenti redditizi e gli imprenditori ad esportare il Made in Italy in Sud America." />
	<meta name="keywords" content="Marco Cello" />
	<meta name="author" content="OGI ADV - @Marticvs" />
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,600,400italic,700' rel='stylesheet' type='text/css'>

<?php wp_head(); ?>
	
	<!-- style -->
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
	<!-- style -->

		<!-- Facebook Pixel Code -->
			<script>
			!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
			n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
			document,'script','https://connect.facebook.net/en_US/fbevents.js');
			fbq('init', '209048056207093', {
			em: 'insert_email_variable,'
			});
			fbq('track', 'PageView');
			</script>
			<noscript>
			<img height="1" width="1" style="display:none"
			src="https://www.facebook.com/tr?id=209048056207093&ev=PageView&noscript=1"
			/>
			</noscript>
		<!-- DO NOT MODIFY -->
		<!-- End Facebook Pixel Code -->

	</head>
	<body>
	<header role="banner" id="fh5co-header">
			<div class="container">
				<!-- <div class="row"> -->
			    <nav class="navbar navbar-default">
		        <div class="navbar-header">
		        	<!-- Mobile Toggle Menu Button -->
					<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
		          	<a class="navbar-brand" href="home"><img class="logo" src="<?php echo get_template_directory_uri(); ?>/images/logoCello.png"></a>
		        </div>

		        <div id="navbar" class="navbar-collapse collapse">
		          <ul class="nav navbar-nav navbar-right">
		            <li class="active"><a href="#" data-nav-section="home"><span>HOME</span></a></li>
		            <li><a href="#" data-nav-section="about"><span>CHI SONO</span></a></li>
		            <li><a href="#" data-nav-section="services"><span>SERVIZI</span></a></li>
		            <li><a href="#" data-nav-section="testimonials"><span>TESTIMONIANZE</span></a></li>
		            <!-- <li><a href="#" data-nav-section="pricing"><span>PRODOTTI E PREZZI</span></a></li> -->
		            <li><a href="#" data-nav-section="features"><span>BLOG</span></a></li>
		            <li><a href="#" data-nav-section="press"><span>CONTATTI</span></a></li>
		          </ul>
		        </div>
			    </nav>
			  <!-- </div> -->
		  </div>
	</header>