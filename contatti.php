<?php 
	//Template Name: Contatti
?>

<?php wp_head(); ?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">

<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,600,400italic,700' rel='stylesheet' type='text/css'>

 <header role="banner" id="fh5co-header">
			<div class="container">

				<!-- <div class="row"> -->
			    <nav class="navbar navbar-default">
		        <div class="navbar-header">
		        	<!-- Mobile Toggle Menu Button -->
					<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
		          	<a class="navbar-brand" href="home"><img class="logo" src="<?php echo get_template_directory_uri(); ?>/images/logoCello.png"></a>
		        </div>
		        <div id="navbar" class="navbar-collapse collapse">
		          <ul class="nav navbar-nav navbar-right">
		          	<li><a href="#" data-nav-section="press"><span>CONTATTI</span></a></li>
		            <li><a href="home"><span>BACK TO HOME</span></a></li>
		          </ul>
		        </div>
			    </nav>
			  <!-- </div> -->
		  </div>
	</header>

	<div id="fh5co-press" data-section="press">
		<div class="container">
			<div class="row">
				<div class="col-md-12 section-heading text-center">
					<h2 class="single-animate animate-press-1">CONTATTI</h2>
				</div>
			</div>
				
<?php echo do_shortcode('[contact-form-7 id="28" title="Modulo di contatto 1"]'); ?>

<label> Il tuo nome (richiesto)
    [text* your-name] </label>

<label> La tua email (richiesto)
    [email* your-email] </label>

<label> Oggetto
    [text your-subject] </label>

<label> Il tuo messaggio
    [textarea your-message] </label>

[submit "Invia"]			</div>
		</div>
	</div>


	<!-- barra icone social -->
		<div class="fixed-info">
				<div class="row">
					<div class="col-md-12 text-center">
						<ul class="social social-circle" style="margin-top: 1%">
							<li><a href="https://www.facebook.com/marcocellobusinessman"><i class="icon-facebook"></i></a></li>
							<li><a href="https://www.youtube.com/channel/UCSqhuFfohkbalFZs23TrGEw"><i class="icon-youtube"></i></a></li>
							<li><a href="https://www.linkedin.com/in/marco-cello-915204125/"><i class="icon-linkedin"></i></a></li>
							<li><a href="https://www.instagram.com/marcocello_businessman/"><i class="icon-instagram"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="row row-bottom-padded-sm">
					<div class="col-md-12">
						<p class="copyright text-center" style="color:#fff";>&copy; 2017 - <a href="http://www.ogiadvertising.it/">OGIadvertising</a> </p>
					</div>
				</div>
		</div>
	<!-- barra icone social -->

<?php get_footer();
?>		